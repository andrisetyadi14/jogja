-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2019 at 02:23 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multi_login`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id_event` int(4) NOT NULL,
  `nama_event` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_wisata` varchar(100) NOT NULL,
  `deskripsi_event` varchar(190) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id_event`, `nama_event`, `tanggal`, `nama_wisata`, `deskripsi_event`, `gambar`) VALUES
(4, 'Labuhan Parangkusumo', '2020-04-06', 'Pantai Ngrenehan', 'Labuhan adalah salah satu upacara adat yang dilakukan oleh Raja-raja di Keraton Yogyakarta. Upacara adat ini bertujuan untuk memohonkan keselamatan Kanjeng Sri Sultan, Kraton Yogyakarta dan ', 'labuhan parangkusumo.jpg'),
(5, 'Sekaten', '2020-11-01', 'Candi gebang', 'Festival Sekaten adalah rangkaian kegiatan tahunan sebagai peringatan Maulid Nabi Muhammad yang diadakan oleh keraton Surakarta dan Yogyakarta. Rangkaian perayaan secara resmi berlangsung da', 'sekaten.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(2) NOT NULL,
  `kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'alam'),
(2, 'museum'),
(3, 'sejarah');

-- --------------------------------------------------------

--
-- Table structure for table `komentar_event`
--

CREATE TABLE `komentar_event` (
  `id_event` int(11) NOT NULL,
  `komentar` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar_event`
--

INSERT INTO `komentar_event` (`id_event`, `komentar`, `username`) VALUES
(3, 'dsvsvsdvs', 'andri'),
(3, 'direkomendasikan untuk berkunjung kesini guys', 'andri'),
(5, 'acaranya sangat rame, sunnguh mengesankan guys', 'panji'),
(5, 'wahhh suasananya sangat rame, dan sangat bagus', 'setya'),
(5, 'hahahhaa', 'user'),
(4, 'inception suka dengan ini', 'inception'),
(4, 'pengen liat :)', 'inception');

-- --------------------------------------------------------

--
-- Table structure for table `komentar_kuliner`
--

CREATE TABLE `komentar_kuliner` (
  `id_kuliner` int(4) NOT NULL,
  `komentar` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar_kuliner`
--

INSERT INTO `komentar_kuliner` (`id_kuliner`, `komentar`, `username`) VALUES
(5, 'tahunya luar biasa sangat krispy', 'andri'),
(6, 'makanannya sangat enak, manis-manis gitu lah', 'panji');

-- --------------------------------------------------------

--
-- Table structure for table `komentar_wisata`
--

CREATE TABLE `komentar_wisata` (
  `id_komentar` int(3) NOT NULL,
  `komentar` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar_wisata`
--

INSERT INTO `komentar_wisata` (`id_komentar`, `komentar`, `username`) VALUES
(3, 'efwf', 'zczvv'),
(3, 'efwf', 'zczvv'),
(10, 'wwgwfwf', 'andri'),
(9, 'sangat banyak pesawat', 'andri'),
(9, 'pesawat-pesawat milik tni', 'panji'),
(16, 'tempatnya sangat bagus, cocok untuk berfoto-foto', 'panji'),
(18, 'alamnya sangat indah nan cantik', 'setya'),
(16, 'hehehe', 'user'),
(22, 'indah', 'user'),
(16, 'candinya sangat indah, menunjukkan zaman dulu banget', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `kuliner`
--

CREATE TABLE `kuliner` (
  `id_kuliner` int(4) NOT NULL,
  `nama_kuliner` varchar(30) NOT NULL,
  `deskripsi_kuliner` varchar(190) NOT NULL,
  `gambar` varchar(190) NOT NULL,
  `alamat` varchar(190) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuliner`
--

INSERT INTO `kuliner` (`id_kuliner`, `nama_kuliner`, `deskripsi_kuliner`, `gambar`, `alamat`) VALUES
(6, 'Cenil', 'Cenil adalah makanan sejenis kue yang dibuat dari pati ketela pohon yang di buat bulat-bulat kecil seukuran kelereng, diberi warna merah dan direbus. Yang unik dari kue cenil ini, selain nam', 'kue cenil.jpg', 'Jl. Bumijo No.52-40, Bumijo, Kec. Jetis, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55231'),
(7, 'Sate Klatak', 'Sate Klatak adalah sate yang berbahan dasar kambing. Yang membedakan sate tersebut dengan sate lainnya adalah pada bumbu untuk pengolahan sate. Sate Klatak tidak menggunakan bumbu kecap atau', 'sate klatak.jpg', 'Jl. Sultan Agung No.18, Jejeran II, Wonokromo, Kec. Pleret, Bantul, Daerah Istimewa Yogyakarta 55791');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(5) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `username`, `password`, `email`) VALUES
(3, 'setya', '789', 'andresetyadi@gmail.com'),
(4, 'panji', '666', 'andrisetyadi7@gmail.com'),
(14, 'inception', 'inception', 'inception@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `id` int(5) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`id`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '', '2019-12-06 08:57:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wisata`
--

CREATE TABLE `wisata` (
  `id_wisata` int(4) NOT NULL,
  `nama_wisata` varchar(30) NOT NULL,
  `alamat_wisata` varchar(190) NOT NULL,
  `deskripsi_wisata` varchar(190) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wisata`
--

INSERT INTO `wisata` (`id_wisata`, `nama_wisata`, `alamat_wisata`, `deskripsi_wisata`, `gambar`, `kategori`) VALUES
(16, 'Candi gebang', 'Gebang, Jl. Jetis, Jetis, Wedomartani, Kec. Ngemplak, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55584', 'Kisah mengenai candi ini berawal pada November 1936, ketika salah satu penduduk mencangkul tanah dan menemukan Arca Ganesha. Lalu, pada  1937 sampai 1939 Prof. Ir. F.R. Van Romondt memugar c', 'candi gebang.jpg', 'sejarah'),
(17, 'Museum Affandi', 'Jl. Laksda Adisucipto No.167, Papringan, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281', 'Museum Affandi merupakan salah satu museum seni di Provinsi Daerah Istimewa Yogyakarta, Indonesia. Museum yang berada di tepi Sungai Gajah Wong ini menyimpan berbagai macam lukisan karya Aff', 'museum-affandi-Yogyakarta.jpg', 'museum'),
(18, 'Pantai Ngrenehan', 'Pantai Ngrenehan, Daerah Istimewa Yogyakarta', 'Pantai Ngrenehan merupakan pantai nelayan. Terletak di desa Kanigoro Kecamatan Saptosari kurang lebih 30 km di sebelah selatan kota Wonosari. Suatu pantai berupa teluk yang dikelilingi hampa', 'pantai ngrenehan.jpg', 'alam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kuliner`
--
ALTER TABLE `kuliner`
  ADD PRIMARY KEY (`id_kuliner`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wisata`
--
ALTER TABLE `wisata`
  ADD PRIMARY KEY (`id_wisata`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id_event` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kuliner`
--
ALTER TABLE `kuliner`
  MODIFY `id_kuliner` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wisata`
--
ALTER TABLE `wisata`
  MODIFY `id_wisata` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
